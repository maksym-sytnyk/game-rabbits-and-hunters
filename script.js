class Target {
   constructor() {
      this.hunters = [];
      this.someData = 0;
   }
   on(callback) {
      this.hunters.push(callback);

      return () => {
         this.hunters = this.hunters.filter((cb) => cb !== callback);
      };
   }
   someChange() {
      this.someData++;
      this.hunters.forEach((cb) => cb(this.someData));
   }
}


class Hunter {
   constructor(target) {
      this.target = target;
      this.targetSomeData = 0;
   }
   watch() {
      return (newSomeData) => {
         console.log(this, 'this');
         this.targetSomeData = newSomeData;
         console.log("Hunter", this.targetSomeData);
      };
   }
   startWatch() {
      this.off = this.target.on(this.watch());
   }
   stopWatch() {
      this.off();
   }
}

let t = new Target();
let h1 = new Hunter(t);
let h2 = new Hunter(t);

h1.startWatch();
h2.startWatch();
t.someChange();
t.someChange();
h1.stopWatch();
h2.stopWatch();
t.someChange();
t.someChange();
console.log(t);
console.log(h1);
console.log(h2);






