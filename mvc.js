class Observer {
  constructor() {
    this.events = {};
  }

  on(event, callback) {
    if (this.events[event]) {
      this.events[event].push(callback)
    } else this.events[event] = [callback]
  }

  off(event, callback) {
    if (this.events[event]) {
      this.events[event] = this.events[event].filter(cb => !(cb == callback));
      if (!this.events[event].length) {
        delete this.events[event]
      }
    }
  }

  emit(event, ...arg) {
    if (this.events[event]) {
      this.events[event].forEach(cb => cb(arg))
    }
  }
}

class Rabbit extends Observer {
  constructor(name) {
    super();
    this.name = name;
    this.x = randomUntilHundred();
    this.y = randomUntilHundred();
    this.isZombi = false;
    this.isDead = false;
  }

  move() {
    this.x = randomUntilHundred();
    this.y = randomUntilHundred();
    this.emit('move', this.x, this.y, this.name, deleteRabbit, makeDead, makeZombi, renderZombiImg);
  }

  get coords() {
    return { x: this.x, y: this.y }
  }

}

Rabbit.rabbits = [];

class Hunter extends Observer {
  constructor(name) {
    super();
    this.name = name;
    this.rabbits = {};
    this.changeCoords = this.changeCoords.bind(this);
    this.fire = this.fire.bind(this);
    this.isWatching = false;
    this.drunkLevel = 0;
    this.lostTarget = false;
    this.hits = 0;
    this.firstShotDone = false;
  }

  changeCoords(arg) {
    this.rabbits = { [arg[2]]: { x: arg[0], y: arg[1] } };
  }

  getCoordsTargetName() {
    if (Object.keys(this.rabbits)) return Object.keys(this.rabbits)[0]
  }

  fire(arg) {
    const targetName = arg[2];
    if (targetName[0] == 'r') {
      const target = Rabbit.rabbits.filter(r => r.name == targetName)[0];
      const luck = randomUntilHundred();
      this.lostTarget = false;
      switch (this.drunkLevel) {
        case 0:
          if (luck < 50) {
            this.hits++;
            if (target.isZombi) {
              arg[4](target);
              this.lostTarget = true;
            } else {
              arg[5](target);
              arg[6](targetName);
            }
          };
          break;
        case 1:
          if (luck < 75) {
            this.hits++;
            if (target.isZombi) {
              arg[4](target);
              this.lostTarget = true;
            } else {
              arg[5](target);
              arg[6](targetName);
            }
          };
          break;
        case 2:
          if (luck < 75) {
            this.hits++;
            if (target.isZombi) {
              arg[4](target);
              this.lostTarget = true;
            } else {
              arg[5](target);
              arg[6](targetName);
            }
          };
          if (!this.firstShotDone) {
            this.firstShotDone = !this.firstShotDone;
            this.fire(arg);
          } else this.firstShotDone = !this.firstShotDone;
          break;
      }
      this.emit('fire', this.x, this.y, this.name, deleteRabbit, makeDead, makeZombi, renderZombiImg);
    } else if (targetName[0] == 'h') {
      const target = Hunter.hunters.find(h => h.name == targetName);
      Hunter.hunters = Hunter.hunters.filter(h => h != target);
      const hunterImg = document.getElementById(targetName);
      if (hunterImg) hunterImg.remove();
    }
  }
}

Hunter.hunters = [];

function randomUntilHundred() {
  return +(Math.random() * 100).toFixed(0);
}

class Forest {

  moveRabbitImg(name, coords) {
    const rabbit = document.getElementById(name);
    if (!rabbit) return;
    rabbit.style.top = coords.y + '%';
    rabbit.style.left = coords.x + '%';
  }

  createRabbitImg(name) {
    const rabbit = document.createElement('div');
    rabbit.className = 'rabbit';
    rabbit.id = name;
    wood.appendChild(rabbit);
  }
}

const forest = new Forest();


function moveRabbitImg(name, coords) {
  const rabbit = document.getElementById(name);
  if (!rabbit) return;
  rabbit.style.top = coords.y + '%';
  rabbit.style.left = coords.x + '%';
}

function createRabbitImg(name) {
  const rabbit = document.createElement('div');
  rabbit.className = 'rabbit';
  rabbit.id = name;
  wood.appendChild(rabbit);
}

function createHunterImg(name) {
  const container = document.createElement('div');
  container.className = 'hunter-container';
  container.id = name;
  container.innerHTML = `<div id="${name + 'img'}" class="hunter"> </div><div id="${name + 'text'}" class="hunter-text"></div>`;
  fireZone.appendChild(container);
  return container;
}

function addButton(type, parent, name) {
  let button = document.createElement('button');
  button.id = name + 'btn';
  if (type == 'drink') {
    button.className = 'drink drink-none';
  } else if (type == 'watch') {
    button.className = 'watch';
  }
  parent.appendChild(button);
  return button;
}

function renderNotification(arg) {
  const hunterText = document.querySelector(`#${this.name} .hunter-text`);
  if (!hunterText) return;
  if (typeof arg == 'object' && arg[1]) {
    hunterText.innerHTML = `Попаданий: <b>${this.hits}</b>. О! Я чую <b>${arg[2]}</b>! Y: ${arg[1]}, X: ${arg[0]}`;
  } else if (arg[1]) {
    hunterText.innerHTML = `Попаданий: <b>${this.hits}</b>. Ну <b>${arg}</b>, ну погоди.. ой это не тот мультик!`;
  } else {
    hunterText.innerHTML = 'Че-то я перебрал.. Ик! ';
  }
}

function deleteRabbit(target) {
  const index = Rabbit.rabbits.indexOf(target);
  Rabbit.rabbits.splice(index, 1);

}

function deleteRabbitImg(rabbitName) {
  const rabbitImg = document.querySelector(`#${rabbitName}`);
  if (rabbitImg) rabbitImg.remove();
}

function makeZombi(target) {
  target.isZombi = true;
}

function makeDead(target) {
  target.isDead = true;
}

function renderZombiImg(rabbitName) {
  const rabbitImg = document.querySelector(`#${rabbitName}`);
  rabbitImg.className = 'zombi';
}

addRabbit.addEventListener('click', function () {
  const rabbit = new Rabbit('r' + randomUntilHundred() + '_' + randomUntilHundred());
  Rabbit.rabbits.push(rabbit);
  createRabbitImg(rabbit.name);
  moveRabbitImg(rabbit.name, { x: randomUntilHundred(), y: randomUntilHundred() })
});

moveRabbit.addEventListener('click', function () {
  Rabbit.rabbits.forEach(rabb => {
    rabb.move();
    moveRabbitImg(rabb.name, rabb.coords);
  });
  Rabbit.rabbits = Rabbit.rabbits.filter(rabb => {
    if (rabb.isDead == false) return true;
    deleteRabbitImg(rabb.name);
  });
  Hunter.hunters.forEach(h => {
    console.log(h);
    if (h.lostTarget || !Rabbit.rabbits.find(r => r.name == h.getCoordsTargetName()) && (h.drunkLevel != 3) && (h.isWatching == true)) {
      startWatching(h.name);
    }
  })
});

function startWatching(arg) {
  let hunter;
  if (arg.target) {
    let hunterName = arg.target.id.slice(0, arg.target.id.length - 3);
    hunter = Hunter.hunters.find(h => h.name == hunterName);
  } else {
    hunter = Hunter.hunters.find(h => h.name == arg);
  }
  if (!hunter && hunter.isWatching) return; //костыль
  const array = Rabbit.rabbits.filter(rabb => rabb.isDead == false);
  const randomRabbit = array[Math.floor(array.length * Math.random())];
  if (!randomRabbit) return;
  hunter.renderNotification = renderNotification.bind(hunter);
  randomRabbit.on('move', hunter.changeCoords);
  randomRabbit.on('move', hunter.fire);
  randomRabbit.on('move', hunter.renderNotification);
  renderNotification.call(hunter, randomRabbit.name);
  document.getElementById(`${hunter.name}img`).classList.add('hunter-watching');
  hunter.isWatching = true;
}

addHunter.addEventListener('click', function () {
  if (Hunter.hunters.length <= 3) {
    const hunter = new Hunter('h' + randomUntilHundred() + '_' + randomUntilHundred());
    createHunterImg(hunter.name);
    Hunter.hunters.push(hunter);
    const drinkBtn = addButton('drink', document.getElementById(hunter.name), hunter.name);
    const watchBtn = addButton('watch', document.getElementById(hunter.name), hunter.name);
    watchBtn.addEventListener('click', startWatching
    );
    drinkBtn.addEventListener('click', function (e) {
      const hunterName = e.target.id.slice(0, e.target.id.length - 3);
      const hunter = Hunter.hunters.find(h => h.name == hunterName);
      switch (hunter.drunkLevel) {
        case 0:
          hunter.drunkLevel++;
          drinkBtn.className = 'drink drink-one';
          break;
        case 1:
          hunter.drunkLevel++;
          drinkBtn.className = 'drink drink-two';
          break;
        case 2:
          hunter.drunkLevel++;
          drinkBtn.className = 'drink drink-three';
          document.querySelector(`#${hunterName}img`).classList.add('hunter-drunk');
          // сделать функцию или переделать функцию startWatching
          // и использовать ее потом после убийства охотника 
          // то есть в конце метода fire

          Rabbit.rabbits.forEach(rabb => {
            rabb.off('move', hunter.changeCoords);
            rabb.off('move', hunter.fire);
            rabb.off('move', hunter.renderNotification);
          });
          const arrayHuntersAgainstRabbits = Hunter.hunters.filter(h => h.drunkLevel != 3);
          const randomHunter = arrayHuntersAgainstRabbits[Math.floor(arrayHuntersAgainstRabbits.length * Math.random())];
          randomHunter.on('fire', hunter.fire);
          randomHunter.on('fire', hunter.renderNotification);
          break;
      }
    })
  } else addHunter.classList.add('prohibition');
});
